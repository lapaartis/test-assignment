$spa.setAppId('app');

// App Routes
$spa.addRoute('/', 'home', 'Select Test');
$spa.addRoute('/questions', 'questions', 'Question');

var user_md5 = null; // User Token variable

// App Controllers
$spa.addController('home', function () {

    var app = $spa.app_id;
    $(app + ' #page_name').html($spa.currentRoute.t);

    // Load user form with fields
    var loadTestSelectView = function () {

        $('#content').html($spa.getLoading());

        $.ajax({
            url: window.location.origin + '/api/get/test-select-view',
            method: 'POST',
            success: function (data) {
                $('#content').html(data);
            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        validationError('#test_select_form', data.responseJSON);
                        break;
                    default:
                        alert(`${data.statusText}: ${data.responseJSON}`);
                }
            }
        });
    };

    // Action on user form button click
    var setBeginClick = function () {
        $(app)
            .off('click', '.test_begin')
            .on('click', '.test_begin', function (event) {
                event.preventDefault();
                var form = $('form#test_select_form').serializeArray();
                $.ajax({
                    url: window.location.origin + '/api/restrict/save/begin-test',
                    method: 'POST',
                    headers: {
                        'Authorization': 'Bearer usertokensecret'
                    },
                    data: form,
                    success: function (data) {
                        if (data.status == true) {
                            user_md5 = data.user;
                            $spa.changePathByController('questions');
                        } else {
                            alert(data.error);
                        }
                    },
                    error: function (data) {
                        switch (data.status) {
                            case 422:
                                validationError('#test_select_form', data.responseJSON);
                                break;
                            default:
                                alert(`${data.statusText}: ${data.responseJSON}`);
                        }
                    }
                });
            });
    };

    // Validation error message display
    // Creating DOM elements ul > li > Error message
    var validationError = function (find, errors) {
        $(find + ' [name]').each(function (idx, el) {
            var feedback = el.nextElementSibling;

            if (feedback != undefined && feedback.classList.contains('feedback')) {
                feedback.parentNode.removeChild(feedback);
            }

            if (errors[el.name] != undefined) {

                var ul = document.createElement("ul");
                ul.classList.add('feedback');

                $.each(errors[el.name], function (index, value) {
                    var li = document.createElement('li');
                    li.append(document.createTextNode(value));
                    ul.append(li);
                });

                insertAfter(ul, el);
            }

        });
    };

    // Insert new DOM element next to current.
    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    // Load features
    loadTestSelectView();
    setBeginClick();
});

$spa.addController('questions', function () {

    var app = $spa.app_id;
    $(app + ' #page_name').html($spa.currentRoute.t);

    var loadTestQuestionView = function () {
        $('#content').html($spa.getLoading());

        $.ajax({
            url: window.location.origin + '/api/get/restrict/question',
            method: 'POST',
            data: {'token': user_md5},
            success: function (data) {

                $('#content').html(data);

                var progressbar = $( "#progressbar" );

                if(progressbar.length == 0) {
                    window.history.pushState("data","Title",'/result/'+user_md5);
                    document.title = 'Result';
                } else {

                    var max = progressbar.attr('data-max') == undefined ? 1 : progressbar.attr('data-max');
                    var current = progressbar.attr('data-current') == undefined ? 0 : progressbar.attr('data-current');

                    progressbar.progressbar({
                        value: (100 / max) * current
                    });
                }

            },
            error: function (data) {
                switch (data.status) {
                    case 422:
                        validationError('#user_question', data.responseJSON);
                        break;
                    default:
                        alert(`${data.statusText}: ${data.responseJSON}`);
                }
            }
        });
    };

    // Action on one of the answer click!

    var setAnswerClick = function () {
        $(app + ' #content')
            .off('click', '.answer-item')
            .on('click', '.answer-item', function (event) {
                // event.preventDefault();

                $(app + ' #content .answer-item').each(function (index, value) {
                    value.classList.remove('selected');
                });

                $(this).addClass('selected');

                $('#answer_input').val($(this).attr('data-target'));

            });
    };

    // Action on next question button press.
    var setNextQuestionClick = function () {
        $(app)
            .off('click', '#next_question')
            .on('click', '#next_question', function (event) {
                event.preventDefault();
                var form = $('form#user_question').serializeArray();
                $.ajax({
                    url: window.location.origin + '/api/restrict/save/user_answer',
                    method: 'POST',
                    data: {
                        'form': form,
                        'token': user_md5
                    },
                    success: function (data) {
                        if (data.status == true) {
                            loadTestQuestionView();
                        } else {
                            alert(data.error);
                        }
                    },
                    error: function (data) {
                        switch (data.status) {
                            case 422:
                                validationError('#feedback', data.responseJSON);
                                break;
                            default:
                                alert(`${data.statusText}: ${data.responseJSON}`);
                        }
                    }
                });
            });
    };

    // Validation error message display
    // Creating DOM elements ul > li > Error message
    var validationError = function (find, errors) {
        $(find).html('');

        var ul = document.createElement("ul");
        ul.classList.add('feedback');
        $.each(errors, function (error_index, error) {
            $.each(error, function (index, value) {
                var li = document.createElement('li');
                li.append(document.createTextNode(value));
                ul.append(li);
            });
        });

        $(find).append(ul);
    };

    setAnswerClick();
    setNextQuestionClick();
    loadTestQuestionView();
});


