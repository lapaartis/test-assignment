var Route = function (p, c, t) {
    this.p = p;
    this.c = c;
    this.t = t;
};
var Controller = function (n, a) {
    this.n = n;
    this.f = a;
};

var $spa = new (function () {
    this.app_id = null;
    this.routes = [];
    this.controllers = [];
    this.currentRoute = null;
    this.currentController = null;

    // Set Application ID
    // @param String n:name
    // @param String c:controller
    this.setAppId = function (id) {
        this.app_id = '#' + id;
    };

    // Add SPA Route
    // @param String n:name
    // @param String c:controller
    this.addRoute = function (n, c, t) {
        this.routes.push(new Route(n, c, t));
    };

    // Find defined route or undefined
    // @param String n:name
    // Return new Route|undefined
    this.findRoute = function (n) {
        return this.routes.find(find => find.p === n);
    };

    // Find defined route by controller name or undefined
    // @param String n:name
    // Return new Route|undefined
    this.findRouteByController = function (n) {
        return this.routes.find(find => find.c === n);
    };

    // Add SPA Controller
    // @param String n:name
    // @param Function f:function
    this.addController = function (n, f) {
        this.controllers.push(new Controller(n, f));
    };

    // Find defined controller or undefined
    // @param String n:name
    // Return new Controller|undefined
    this.findController = function (n) {
        return this.controllers.find(find => find.n === n);
    };

    // Change Path by pathname
    // @param String pathname
    this.changePath = function (pathname) {
        window.history.pushState("data","Title",pathname);
        var route = $spa.findRoute(pathname);
        if(route != undefined) {
            document.title = route.p === undefined ? document.title : route.t;
            $spa.execute(window.location.pathname);
        }
    };

    // Change URL and run this route controller
    // by controller name
    // @param String name
    this.changePathByController = function (name) {

        var route = $spa.findRouteByController(name);
        if(route != undefined) {
            window.history.pushState("data","Title",route.p);
            document.title = route.t === undefined ? document.title : route.t;
            $spa.execute(window.location.pathname);
        } else {
            console.error('Undefined Controller');
        }

    };

    // Show elements if spe-show attribute:
    // if spa-show == true element is showing
    // if spa-show == false element is not showing
    // @param String c_came:controller defined name
    this.spaShow = function (c_name) {
        $('[spa-show]').each(function( idx, el ) {
            if(el.getAttribute('spa-show') == c_name) {
                el.style.display = "block";
            } else {
                el.style.display = "none";
            }
        });
    };

    // Execute SPA public
    // @param String pathname
    this.execute = function (pathname) {
        this.currentRoute = this.findRoute(pathname);
        document.title = this.currentRoute.t === undefined ? document.title : this.currentRoute.t;
        if (this.currentRoute !== undefined) {
            this.currentController = this.findController(this.currentRoute.c);
            if (this.currentController !== undefined) {
                $spa.spaShow(this.currentController.n);
                this.currentController.f(); // Run controller function()
            } else {
                console.error(`Controller "${this.currentRoute.c}" not found!`);
            }
        } else {
            console.error(`Route "${pathname}" not found!`);
        }
    };

    // Return loading animation template
    // Return string template
    this.getLoading = function(){
        return '<div class="lds-wrap"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>';
    };

    // Ready function like JSON $(document).ready();
    // @param String callback:function
    var ready = function(callback){
        // in case the document is already rendered
        if (document.readyState!='loading') callback();
        // modern browsers
        else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
        // IE <= 8
        else document.attachEvent('onreadystatechange', function(){
            if (document.readyState=='complete') callback();
        });
    };

    // Action on history back
    // @param Event event
    window.onpopstate = function(event) {
        console.log('asdfasdf');
        $spa.execute(window.location.pathname);
    };

    ready(function(){
        $spa.execute(window.location.pathname);
    });
});

(function(history){
    var pushState = history.pushState;
    history.pushState = function(state) {
        if (typeof history.onpushstate == "function") {
            history.onpushstate({state: state});
        }
        return pushState.apply(history, arguments);
    }
})(window.history);