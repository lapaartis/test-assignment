# Printful tests

Projekta izstrādes vide.

* XAMPP v3.2.2
* PHP 7.2.11 
* Composer version 1.7.2 
* MySql 10.1.36-MariaDB

Projektā izmantotās composer bibliotēkas:
* illuminate/database": "^5.7",
* "illuminate/events": "^5.7",
* "josegonzalez/dotenv": "^3.2",
* "symfony/routing": "^4.1",
* "symfony/http-foundation": "^4.1",
* "twig/twig": "^2.5",
* "symfony/debug": "^4.1"
   
Ko darīt, lai palaistu projektu:
* Sagatavot projekta vidi. Es izmantoju XAMPP v3.2.2
* Izveidot datubāzi projektam un tā lietotāju.
```sh
mysql -u root -p
CREATE DATABASE database-name;
GRANT ALL PRIVILEGES ON database-name.* TO 'user'@'localhost' IDENTIFIED BY 'your-password;
```
* Noklonēt ar git clone no https://bitbucket.org/lapaartis/test-assignment/src/master/
* palaist komandu composer update
* Pārkopēt .env.example uz .env un definet datubāzes datus
    * DB_DATABASE
    * DB_USERNAME
    * DB_PASSWORD
* Pārkopēt .env.example uz .env un definet datubāzes datus
* Palaižam sava veida migrāciju ar komandām no root:
```sh
$ php create-tables.php
$ php seed-tables.php
```
* Ja ir veiksmīgi nodefinets .env datubāzes dati, tad pēc komandas polaišanas attēlojas paziņojumi::
```sh
[aleaflv@server26 printful.kods.lv]$ php create-tables.php
string(20) "Table created: Tests"
string(20) "Table created: Users"
string(24) "Table created: Questions"
string(22) "Table created: Answers"
string(25) "Table created: User Tests"
string(27) "Table created: User Answers"
string(36) "Database tables created Succesfully!"
```
* Ja ir nepieciešams nodzēst datubāzes datus un tabulas ar drop-table.php to var izdarīt
```sh
$ php drop-tables.php
$ php create-tables.php
$ php seed-tables.php
```
* Projekta palaišana sākas no /public mapes. 
    * Lai palaistu no Apache būs jānodefinē VirtualHost fails ar DocumentRoot uz C:/path/to/project/public
    * Lai palaistu no komandrindas:
```sh
$ cd /public
$ php `php -S localhost:8080`
```
* Projects ir uzlikts. 
 