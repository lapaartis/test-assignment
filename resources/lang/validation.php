<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class.
    |
    */

    'array' => ':attribute vērtībai jābūt masīvam.',
    'currency' => ':attribute vērtībai jābūt valūtas formā.',
    'equal' => ':attribute vērtībai jābūt vienādam ar :equal.',
    'integer' => ':attribute vērtībai jābūt veselam skaitlim.',
    'float' => ':attribute vērtībai jābūt daļskaitlim.',
    'min' => [
        'numeric' => ':attribute vērtībai jābūt vismaz :min.',
        'string' => ':attribute vērtībai jābūt vismaz :min simbolus garam.',
    ],
    'max' => [
        'numeric' => ':attribute vērtībai nedrīkst būt lielākam par :max.',
        'string' => ':attribute vērtībai nedrīkst būt garāks par :max simboliem.',
    ],
    'required' => ':attribute vērtībai ir jabūt norādītai.',
    'string' => ':attribute vērtībai jābūt teksta rindai.',
    'unique' => ':attribute vērtība jau ir aizņēmta.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines.
    |
    */

    'custom' => [
        'test_not_found' => 'Šis tests netika atrasts!',
        'user_has_not_been_created' => 'Persona netika izveidota!',
    ],


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | Swap attribute place-holders with something more reader friendly
    | such as E-Mail Address instead of "email".
    |
    */

    'attributes' => [
//        'some_name' => 'Custom name',
        'user_name' => 'Personas vārda',
        'test_select' => 'Izvēlētā testa',
        'answer' => 'Atbildes',
        'question' => 'Jautājuma',
        'form' => 'Formas',
        'token' => 'Personas atslēgai',
    ],
];