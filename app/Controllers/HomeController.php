<?php

namespace App\Controllers;

use App\Models\Test;
use App\Models\User;

class HomeController extends Controller
{

    /**
     * Render first page of app
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function index($request, $args)
    {
        $tests = Test::all();

        return $this->render('home.twig', array('tests' => $tests));
    }

    /**
     * Render result page
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function result($request, $args)
    {
        $params = $args->toArray();

        if(arr_key('token', $params)) {

            $user = User::where('token', $params['token'])->with('user_test')->first();

            if(isset($user)) {

                return $this->render('result-view.twig', [
                    'user_name' => $user->name,
                    'question_count' => $user->user_test->total_answers,
                    'right_answer_count' => $user->user_test->right_answers,
                ]);
            }

        } else {
            return $this->redirect('');
        }

        // return result view

    }

    /**
     * Redirect any unregistred route.
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function all($request, $args)
    {
        return $this->redirect('');
    }

}