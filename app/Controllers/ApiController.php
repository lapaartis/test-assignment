<?php

namespace App\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Test;
use App\Models\UserAnswer;
use App\Models\UserTest;
use App\Classes\Validation;
use App\Models\User;

class ApiController extends Controller
{

    /**
     * Get User form view with app tests
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function getTestSelectView($request, $args)
    {
        $tests = Test::all();

        return $this->render('test-select.twig', array('tests' => $tests));
    }

    /**
     * Save user and start test with user token
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return json Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveBeginTest($request, $args)
    {

        $request_data = $request->request->all();

        $validate = new Validation();
        $form_values = null;
        $form = collect();
        $status = false;
        $error = '';
        $user_md5 = null;

        // Validate if array exists
        $validate->name('form')->value($request_data)->pattern('array')->required();

        if (!$validate->isSuccess()) {
            return $this->json('Form was not received', 400);
        }

        // Build form values
        foreach ($request_data as $key => $item) {
            $form->put($key, $item);
        }
        $form = $form->toArray();

        // Validate
        $validate->name('user_name')->value(arr_key('user_name', $form, ''))->pattern('string')->required();
        $validate->name('test_select')->value(arr_key('test_select', $form, ''))->pattern('string')->required();

        if (!$validate->isSuccess()) {
            return $this->json($validate->getErrors(), 422);
        }

        $test = Test::where('id', $form['test_select'])->first();

        if (isset($test)) {

            $user = new User();
            $user->name = $form['user_name'];
            $status = $user->save();

            if ($status) {
                $user_md5 = md5($user->id);
                $user->token = $user_md5;
                $status = $user->save();
            }
            if ($status) {

                $status = $user->save();

                $user_test = new UserTest();
                $user_test->user_id = $user->id;
                $user_test->test_id = $test->id;
                $status = $user_test->save();
            } else {
                $error = validation_trans('custom.user_has_not_been_created');
            }

        } else {
            $error = validation_trans('custom.test_not_found');
        }
        return $this->json(['status' => $status, 'error' => $error, 'user' => $user_md5]);
    }

    /**
     * Get next question for user.
     * Complicated checking for next shaffled question
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function getQuestion($request, $args)
    {
        $request_data = $request->request->all();
        $response_question = collect();
        $answers = collect();
        $questions_ended = false;
        $user_answers_count = 0;
        $questions_count = 1;

        if (arr_key('token', $request_data)) {

            $user = User::where('token', $request_data['token'])->first();

            if (isset($user)) {

                $last_user_test = UserTest::where('user_id', $user->id)->first();

                if (isset($last_user_test)) {

                    $test = Test::where('id', $last_user_test->test_id)->first();

                    if (isset($test)) {

                        $user_answers = UserAnswer::where('user_test_id', $last_user_test->id)->get();
                        $questions = Question::where('test_id', $test->id)->get();

                        // get count of questions & user answers
                        $questions_count = $questions->count();
                        $user_answers_count = $user_answers->count();

                        // Check if questions exists
                        if ($questions !== null && $questions->count() > 0) {

                            $no_answered_questions = collect();

                            // Get user un-answered questions
                            foreach ($questions as $question) {

                                $user_answer = $user_answers->where('question_id', $question->id);

                                if ($user_answer->count() === 0) {

                                    $no_answered_questions->push($question);

                                }
                            }

                            // Shuffle questions
                            $no_answered_questions = $no_answered_questions->shuffle();

                            $response_question = $no_answered_questions->first();

                            if ($response_question !== null && isset($response_question)) {

                                $answers = collect();

                                $db_answers = Answer::where('question_id', $response_question->id)->get();
                                // Get user un-answered questions
                                foreach ($db_answers as $answer) {

                                    $answers->push($answer);
                                }

                                $answers = $answers->shuffle();

                            } else {


                                if ($questions_count == $user_answers_count) {

                                    $right_answer_count = 0;

                                    $test_answers = Answer::where('test_id', $test->id)->get();

                                    foreach ($user_answers as $user_answer) {

                                        $right_test_answer = $test_answers->where('id', $user_answer->answer_id)->where('right', true);

                                        if ($right_test_answer->count() > 0) {
                                            $right_answer_count++;
                                        }
                                    }

                                    // Save user test results
                                    $last_user_test->right_answers = $right_answer_count;
                                    $last_user_test->total_answers = $questions_count;
                                    $last_user_test->save();

                                    // return result view
                                    return $this->render('result.twig', [
                                        'user_name' => $user->name,
                                        'question_count' => $questions_count,
                                        'right_answer_count' => $right_answer_count
                                    ]);

                                } else {
                                    return $this->json('Current question null', 404);
                                }
                            }

                        } else {
                            return $this->json('Questions not found', 404);
                        }

                    } else {
                        return $this->json('Test not found', 404);
                    }

                } else {
                    return $this->json('User test not found', 404);
                }

            } else {
                return $this->json('User not found', 404);
            }

        } else {
            return $this->json('Token not requested', 404);
        }

        return $this->render('question.twig', [
            'question' => $response_question,
            'answers' => $answers,
            'questions_count' => $questions_count,
            'user_answers_count' => $user_answers_count
        ]);
    }

    /**
     * Save user answers
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return json Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveAnswer($request, $params)
    {
        $request_data = $request->request->all();
        $status = false;
        $user_md5 = null;
        $error = '';
        $user = null;
        $user_test = null;
        $test = null;
        $question = null;
        $question_id = null;
        $answer = null;
        $answer_id = null;
        $validate = new Validation();
        $valid_question = false;
        $valid_answer = false;

        /*
         * --------------------------------------------------------------------
         * Validate request
         * --------------------------------------------------------------------
         * */

        $validate->name('form')->value($request_data)->pattern('array')->required();
        $token_req = arr_key('token', $request_data) ? $request_data['token'] : null;
        $validate->name('token')->value($token_req)
            ->pattern('string')->required();

        if (arr_key('form', $request_data)) {

            $validate->name('form')->value($request_data['form'])->pattern('array')->required();

            if (is_array($request_data['form'])) {

                foreach ($request_data['form'] as $input) {

                    if ($input['name'] == 'question') {
                        $validate->name('question')->value($input['value'])->pattern('string')->required();
                        $question_id = $input['value'];
                        $valid_question = true;
                    }
                    if ($input['name'] == 'answer') {
                        $validate->name('answer')->value($input['value'])->pattern('string')->required();
                        $answer_id = $input['value'];
                        $valid_answer = true;
                    }
                }
                if ($valid_question == false) {
                    $validate->name('question')->value(null)->pattern('string')->required();
                }
                if ($valid_answer == false) {
                    $validate->name('answer')->value(null)->pattern('string')->required();
                }
            } else {
                $error = 'Form variables is not array!';
            }

        } else {
            $error = 'Form not exists!';
        }


        // return errors if exists
        if (!$validate->isSuccess()) {
            return $this->json($validate->getErrors(), 422);
        }

        /*
         * --------------------------------------------------------------------
         * Begin answer saving
         * --------------------------------------------------------------------
         * */

        $question = Question::where('id', $question_id)->first();
        $answer = Answer::where('id', $answer_id)->first();
        $user = User::where('token', $request_data['token'])->first();

        if (isset($user)) {

            if (isset($question)) {
                $test = Test::where('id', $question->test_id)->first();
                if (isset($test)) {
                    $user_test = UserTest::where('id', $user->id)->first();
                    if (isset($user_test)) {
                        if (isset($answer)) {

                            $new = new UserAnswer();
                            $new->user_test_id = $user_test->id;
                            $new->question_id = $question->id;
                            $new->answer_id = $answer->id;
                            $status = $new->save();

                        } else {
                            return $this->json('Answer not found', 404);
                        }
                    } else {
                        return $this->json('User Test not found', 404);
                    }
                } else {
                    return $this->json('Test not found', 404);
                }
            } else {
                return $this->json('Question not found', 404);
            }
        } else {
            return $this->json('User not found', 404);
        }

        return $this->json(['status' => $status, 'error' => $error]);
    }

    /**
     * Get User result after finishing. Return only
     * inner tamplate of result.
     *
     * @param object $request    The request data
     * @param array   $args  The request parameters
     *
     * @return string The rendered template
     */
    public function result($request, $args)
    {
        $params = $args->toArray();
        $error = '';
        if(arr_key('token', $params)) {

            $user = User::where('token', $params['token'])->with('user_test')->first();

            if(isset($user)) {

                return $this->render('result.twig', [
                    'user_name' => $user->name,
                    'question_count' => $user->user_test->total_answers,
                    'right_answer_count' => $user->user_test->right_answers,
                ]);

            } else {
                $error = 'User not found!';
            }

        } else {
            $error = 'Token not found!';
//            return $this->redirect('');
        }
        return $this->json(['status' => false, 'error' => $error]);
        // return result view

    }
}