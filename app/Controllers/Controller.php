<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Controller extends Twig_Environment
{
    public $twig;
    protected $registry = [];

    /**
     * Creates new Controller instance.
     * Set up route features
     *
     */
    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem('resources/views', base_path());

        $this->twig = parent::__construct($loader, array(
            'cache' => base_path('storage'),
            'debug' => true,
            'strict_variables' => true
        ));
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        $this->addGlobal('base_path', $protocol.'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT']);
//        $this->twig->addGlobal('base_path', $_SERVER[HTTP_HOST]);

    }

    /**
     * Create and return JsonResponse
     * @param mixed $data    The response data
     * @param int   $status  The response status code
     * @param array $headers An array of response headers
     * @param bool  $json    If the data is already a JSON string
     * @return json Symfony\Component\HttpFoundation\JsonResponse
     */
    static function json($data = null, int $status = 200, array $headers = array(), bool $json = false)
    {
        return new JsonResponse($data, $status, $headers, $json);
    }

    /**
     * Set up redirect feature of undefined $data & defined
     * @param mixed $data    The response data
     * @return json Symfony\Component\HttpFoundation\RedirectResponse
     */
    static function redirect($data = null)
    {
        if(filter_var($data, FILTER_VALIDATE_URL) === false) {
            $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
            $data =  $protocol.'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        }

        return new RedirectResponse($data);
    }


}