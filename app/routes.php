<?php



use App\Classes\Router;

$route = new Router();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application.
|
*/

// Master page
$route->get('/', 'App\Controllers\HomeController::index');

// User pages
$route->post('/api/get/test-select-view', 'App\Controllers\ApiController::getTestSelectView');
$route->get('/result/{token}', 'App\Controllers\HomeController::result');
$route->post('/api/restrict/save/begin-test', 'App\Controllers\ApiController::saveBeginTest');
// Questions pages
$route->post('/api/get/restrict/question', 'App\Controllers\ApiController::getQuestion');
$route->post('/api/restrict/save/user_answer', 'App\Controllers\ApiController::saveAnswer');

// Result page
$route->get('/api/result/{token}', 'App\Controllers\ApiController::result');

// Any route handler
$route->get('/{req}', 'App\Controllers\HomeController::all');


/*
|--------------------------------------------------------------------------
| Get routes and return for execute
|--------------------------------------------------------------------------
*/
return $route->getRoutes();




