<?php

namespace App\Classes;


use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Router
{
    /**
     * The route collection instance.
     *
     * @var \Illuminate\Routing\RouteCollection
     */
    protected $routes;

    public function __construct()
    {
        $this->routes = new RouteCollection;

//        $this->uri = $uri;
//        $this->methods = (array) $methods;
//        $this->action = $this->parseAction($action);
//
//        if (in_array('GET', $this->methods) && ! in_array('HEAD', $this->methods)) {
//            $this->methods[] = 'HEAD';
//        }
//
//        if (isset($this->action['prefix'])) {
//            $this->prefix($this->action['prefix']);
//        }
    }

    /**
     * Register a new GET route with the router.
     *
     * @param  string $uri
     * @param  \Closure|array|string|null $action
     * @return \Illuminate\Routing\Route
     */
    public function get($uri, $action = null, $name = null)
    {
        $this->addRoute(['GET', 'HEAD'], $uri, $action, $name);

        return $this;
    }

    /**
     * Register a new POST route with the router.
     *
     * @param  string $uri
     * @param  \Closure|array|string|null $action
     * @return \Illuminate\Routing\Route
     */
    public function post($uri, $action = null, $name = null)
    {
        $this->addRoute(['POST'], $uri, $action, $name);

        return $this;
    }

    /**
     * Add a route to the underlying route collection.
     *
     * @param  array|string $methods
     * @param  string $uri
     * @param  \Closure|array|string|null $action
     * @param  string $name
     * @return \Illuminate\Routing\Route
     */
    public function addRoute(array $methods, String $uri, $action, $name)
    {
        return $this->routes->add(is_null($name) ? $uri : $name,
            new Route(
                $uri,
                array(
                    '_controller' => $action,
                    'path' => '/',
                    'permanent' => 'true'
                ),
                array(
                    'req' => ".+"
                ),
                array(),
                '',
                array(),
                $methods,
                ''
            ));
    }
    /**
     * Return all defined routes
     *
     * @return \Illuminate\Routing\Route
     */
    public function getRoutes()
    {
        return $this->routes;
    }

}