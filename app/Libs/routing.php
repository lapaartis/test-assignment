<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\Debug\DebugClassLoader;

if(arr_key('APP_DEBUG', $_ENV) ? $_ENV['APP_DEBUG'] : false) {
    DebugClassLoader::enable();
    ExceptionHandler::register();
    ErrorHandler::register();
    Debug::enable();
}

$request = Request::createFromGlobals();


$routes = include base_path('app/routes.php');

$context = new RequestContext();

$context->fromRequest($request);

$matcher = new UrlMatcher($routes, $context);

$parameters = $matcher->matchRequest($request);

$request_params = collect();
foreach ($parameters as $key => $item) {
    if(!($key == '_controller' || $key == '_route')) {
        $request_params->put($key, $item);
    }
}

list($controllerClassName, $action) = explode('::',$parameters['_controller']);
$controller = new $controllerClassName();
$response = $controller->{$action}($request, $request_params);

if(is_string($response)) {
    $response = new Response($response);
}

$response->send();

