<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    /**
     * Get all of the answers for the question.
     */
    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'question_id', 'id');
    }

    /**
     * Get the test that owns the user answer.
     */
    public function test()
    {
        return $this->belongsTo('App\Models\Test', 'test_id', 'id');
    }
}