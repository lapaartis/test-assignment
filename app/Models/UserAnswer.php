<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $table = 'user_answers';

    /**
     * Get the user that owns the user answer.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Get the answer that owns the user answer.
     */
    public function answer()
    {
        return $this->belongsTo('App\Models\Answer', 'answer_id', 'id');
    }
}