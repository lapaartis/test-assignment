<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    /**
     * Get all of the useranswers for the question.
     */
    public function user_answers()
    {
        return $this->hasMany('App\Models\UserAnswer', 'answer_id', 'id');
    }

    /**
     * Get the question that owns the user answer.
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id', 'id');
    }
}