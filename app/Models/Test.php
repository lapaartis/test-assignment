<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'tests';

    /**
     * Get all of the questions for the test.
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'test_id', 'id');
    }
}