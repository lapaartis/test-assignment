<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTest extends Model
{
    protected $table = 'user_tests';

    /**
     * Get the user that owns the user answer.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Get the test that owns the user test.
     */
    public function test()
    {
        return $this->belongsTo('App\Models\Test', 'test_id', 'id');
    }
}