<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    /**
     * Get all of the user answers for the user.
     */
    public function user_answers()
    {
        return $this->hasMany('App\Models\UserAnswer', 'user_id', 'id');
    }

    /**
     * Get user test of user
     */
    public function user_test()
    {
        return $this->hasOne('App\Models\UserTest', 'user_id', 'id');
    }

    /**
     * Get user by token
     */
    public function auth_token($token)
    {
        return User::where('token', $token)->first();
    }
}