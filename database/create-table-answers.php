<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('answers', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->string('name')->default('');
    $table->integer('question_id')->unsigned();
    $table->integer('test_id')->unsigned();
    $table->boolean('right')->default(false);
    $table->timestamps();

    // FOREIGN
    $table->foreign('question_id')->references('id')->on('questions');
    $table->foreign('test_id')->references('id')->on('tests');

    // INDEXES
    $table->index(['test_id']);
    $table->index(['question_id', 'test_id']);
});
var_dump('Table created: Answers');