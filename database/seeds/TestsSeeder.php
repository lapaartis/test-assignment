<?php

use App\Models\Test;
use App\Models\Question;
use App\Models\Answer;

$array = require __DIR__.'/TestsArray.php';

foreach ($array as $test_item) {

    $test = new Test();
    $test->name = $test_item['name'];
    $test->save();

    foreach ($test_item['questions'] as $question_item) {
        $question = new Question();
        $question->name = $question_item['name'];
        $question->test_id = $test->id;
        $question->save();

        foreach ($question_item['answers'] as $answer_item) {
            $answer = new Answer();
            $answer->name = $answer_item['name'];
            $answer->right = $answer_item['right'];
            $answer->question_id = $question->id;
            $answer->test_id = $test->id;
            $answer->save();
        }
    }
}
