<?php

return [
    [
        'name' => 'Matemātika',
        'questions' => [
            [
                'name' => '25x40=?',
                'answers' => [
                    [
                        'name' => '800',
                        'right' => false,
                    ],[
                        'name' => '900',
                        'right' => false,
                    ],[
                        'name' => '1000',
                        'right' => true,
                    ],[
                        'name' => '100',
                        'right' => false,
                    ],
                ]
            ],
            [
                'name' => '1000x1000-1000-999x1000=?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],[
                        'name' => '2',
                        'right' => false,
                    ],[
                        'name' => '3',
                        'right' => false,
                    ],[
                        'name' => '4',
                        'right' => false,
                    ],[
                        'name' => '5',
                        'right' => false,
                    ],[
                        'name' => '0',
                        'right' => true,
                    ],
                ]
            ],
            [
                'name' => 'Laiks ir 9.30, cik liels leņķis ir starp stundas rādītāju un minūšu rādītāju?',
                'answers' => [
                    [
                        'name' => '30',
                        'right' => false,
                    ],[
                        'name' => '45',
                        'right' => false,
                    ],[
                        'name' => '90',
                        'right' => false,
                    ],[
                        'name' => '105',
                        'right' => true,
                    ],
                ]
            ],
            [
                'name' => '12:3(3+1)=?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],[
                        'name' => '16',
                        'right' => true,
                    ],
                ]
            ],
            [
                'name' => 'Kurš skaitlis ir nākamais: 12, 11, 13, 10, 14, 9, _?',
                'answers' => [
                    [
                        'name' => '13',
                        'right' => false,
                    ],[
                        'name' => '14',
                        'right' => false,
                    ],[
                        'name' => '15',
                        'right' => true,
                    ],[
                        'name' => '16',
                        'right' => false,
                    ],
                ]
            ],
            [
                'name' => '41,1-40,3=?',
                'answers' => [
                    [
                        'name' => '0,2',
                        'right' => false,
                    ],[
                        'name' => '0,8',
                        'right' => true,
                    ],[
                        'name' => '1,2',
                        'right' => false,
                    ],[
                        'name' => '1,8',
                        'right' => false,
                    ],
                ]
            ],
            [
                'name' => '800:0,25=?',
                'answers' => [
                    [
                        'name' => '200',
                        'right' => false,
                    ],[
                        'name' => '3200',
                        'right' => true,
                    ],
                ]
            ],
            [
                'name' => '2+2x2=?',
                'answers' => [
                    [
                        'name' => '2',
                        'right' => false,
                    ],[
                        'name' => '4',
                        'right' => false,
                    ],[
                        'name' => '6',
                        'right' => true,
                    ],[
                        'name' => '8',
                        'right' => false,
                    ],
                ]
            ],
            [
                'name' => 'Cik ir viena puse no vienas ceturtdaļas no vienas desmitās no 200?',
                'answers' => [
                    [
                        'name' => '2',
                        'right' => false,
                    ],[
                        'name' => '2,5',
                        'right' => true,
                    ],[
                        'name' => '5',
                        'right' => false,
                    ],[
                        'name' => '5,5',
                        'right' => false,
                    ],
                ]
            ]
        ]
    ],
    [
        'name' => 'Vienkrāšie',
        'questions' => [
            [
                'name' => 'Kas seko pēc centimetriem?',
                'answers' => [
                    [
                        'name' => 'Milimetri',
                        'right' => false,
                    ],
                    [
                        'name' => 'Decimetri',
                        'right' => true,
                    ],
                    [
                        'name' => 'Metri',
                        'right' => false,
                    ],
                    [
                        'name' => 'Kilometri',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Cik valsts valodas ir Šveicē?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'No kuras valsts ASV nopirka Aļasku?',
                'answers' => [
                    [
                        'name' => 'No Ķīnas',
                        'right' => false,
                    ],
                    [
                        'name' => 'No Japānas',
                        'right' => false,
                    ],
                    [
                        'name' => 'No Krievijas',
                        'right' => true,
                    ],
                    [
                        'name' => 'No Francijas',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kāds bija Merilinas Monro īstais vārds?',
                'answers' => [
                    [
                        'name' => 'Norma Džīna Monro',
                        'right' => false,
                    ],
                    [
                        'name' => 'Merilina Andersone',
                        'right' => false,
                    ],
                    [
                        'name' => 'Norma Džīna Mortensone',
                        'right' => true,
                    ],
                    [
                        'name' => 'Merilina Monro',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kas ir Kongo Demokrātiskās Republikas galvaspilsēta?',
                'answers' => [
                    [
                        'name' => 'Kinšasa',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nairobi',
                        'right' => false,
                    ],
                    [
                        'name' => 'Librevila',
                        'right' => false,
                    ],
                    [
                        'name' => 'Abidžana',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kurā valstī piedzima Rianna?',
                'answers' => [
                    [
                        'name' => 'ASV',
                        'right' => false,
                    ],
                    [
                        'name' => 'Kanādā',
                        'right' => false,
                    ],
                    [
                        'name' => 'Barbadosā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Trinidadā un Tobago',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Ar cik valstīm Latvijai ir kopīga sauszemes robeža?',
                'answers' => [
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => true,
                    ],
                    [
                        'name' => '5',
                        'right' => false,
                    ]
                ]
            ]
        ]
    ],
    [
        'name' => 'Eģiptiešu dievi',
        'questions' => [
            [
                'name' => 'Kas bija seno ēģiptiešu saules dievs?',
                'answers' => [
                    [
                        'name' => 'Tots',
                        'right' => false,
                    ],
                    [
                        'name' => 'Hors',
                        'right' => false,
                    ],
                    [
                        'name' => 'Ra',
                        'right' => true,
                    ],
                    [
                        'name' => 'Ozīriss',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai kaķus senajā Ēģiptē uzskatīja par svētiem?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai dievu Sobeku senie ēģiptieši attēloja ar ibisa galvu?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => false,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija seno ēģiptiešu rakstības, skaitīšanas un gudrības dievs?',
                'answers' => [
                    [
                        'name' => 'Sobeks',
                        'right' => false,
                    ],
                    [
                        'name' => 'Tots',
                        'right' => true,
                    ],
                    [
                        'name' => 'Amons',
                        'right' => false,
                    ],
                    [
                        'name' => 'Hors',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai senajā Ēģiptē bija politeisms?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija seno ēģiptiešu ūdens un krokodilu dievs?',
                'answers' => [
                    [
                        'name' => 'Anubiss',
                        'right' => false,
                    ],
                    [
                        'name' => 'Amons',
                        'right' => false,
                    ],
                    [
                        'name' => 'Sebeks',
                        'right' => true,
                    ],
                    [
                        'name' => 'Ozīriss',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai senie ēģiptieši ticēja, ka Hora acs amulets dziedina un aizsargā ar labo maģiju?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai senie ēģiptieši ticēja, ka skarabeja amulets padara ļaunu un piešķir maģiskas spējas?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => false,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija seno ēģiptiešu pazemes dievs, mirušo aizbildnis un ievadītājs pēc nāves dzīvē?',
                'answers' => [
                    [
                        'name' => 'Sets',
                        'right' => false,
                    ],
                    [
                        'name' => 'Anubiss',
                        'right' => true,
                    ],
                    [
                        'name' => 'Hatora',
                        'right' => false,
                    ],
                    [
                        'name' => 'Amons',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai senie ēģiptieši ticēja, ka zemes dieva Geba smiekli izraisa zemestrīces?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija seno ēģiptiešu tuksneša, haosa, kara un ārzemnieku dievs?',
                'answers' => [
                    [
                        'name' => 'Gebs',
                        'right' => false,
                    ],
                    [
                        'name' => 'Ozīriss',
                        'right' => false,
                    ],
                    [
                        'name' => 'Amons',
                        'right' => false,
                    ],
                    [
                        'name' => 'Sets',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'Vai senie ēģiptieši uzskatīja, ka dēmoni ir tikpat vareni kā dievi?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => false,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'Vai senie ēģiptieši dievietes pārsvarā attēloja ar gaišu vai dzeltenīgu ādas krāsu?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija seno ēģiptiešu mīlestības, skaistuma, auglības un mūzikas dieviete?',
                'answers' => [
                    [
                        'name' => 'Hatora',
                        'right' => true,
                    ],
                    [
                        'name' => 'Izīda',
                        'right' => false,
                    ],
                    [
                        'name' => 'Basteta',
                        'right' => false,
                    ],
                    [
                        'name' => 'Nuta',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai faraons pēc nāves varēja kļūt par dievu, ja viņa sirds bija smagāka par spalvu?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => false,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => true,
                    ]
                ]
            ],
            [
                'name' => 'Kas bija mirušo, pazemes un augšām celšanās dievs?',
                'answers' => [
                    [
                        'name' => 'Sets',
                        'right' => false,
                    ],
                    [
                        'name' => 'Ozīriss',
                        'right' => true,
                    ],
                    [
                        'name' => 'Amons',
                        'right' => false,
                    ],
                    [
                        'name' => 'Gebs',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai Anubisu kā pazemes dievu ar laiku aizstāja ar Ozīrisu?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ],
            [
                'name' => 'Vai sieviete varēja kļūt par faraoni?',
                'answers' => [
                    [
                        'name' => 'Jā',
                        'right' => true,
                    ],
                    [
                        'name' => 'Nē',
                        'right' => false,
                    ]
                ]
            ]
        ]
    ],
    [
        'name' => 'Atbilžu skaits',
        'questions' => [
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => false,
                    ],
                    [
                        'name' => '5',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => false,
                    ],
                    [
                        'name' => '5',
                        'right' => false,
                    ],
                    [
                        'name' => '6',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => false,
                    ],
                    [
                        'name' => '5',
                        'right' => false,
                    ],
                    [
                        'name' => '6',
                        'right' => false,
                    ],
                    [
                        'name' => '7',
                        'right' => true,
                    ],
                ],
            ],
            [
                'name' => 'Cik atbildes ir attēlotas?',
                'answers' => [
                    [
                        'name' => '1',
                        'right' => false,
                    ],
                    [
                        'name' => '2',
                        'right' => false,
                    ],
                    [
                        'name' => '3',
                        'right' => false,
                    ],
                    [
                        'name' => '4',
                        'right' => false,
                    ],
                    [
                        'name' => '5',
                        'right' => false,
                    ],
                    [
                        'name' => '6',
                        'right' => false,
                    ],
                    [
                        'name' => '7',
                        'right' => false,
                    ],
                    [
                        'name' => '8',
                        'right' => true,
                    ],
                ],
            ],
        ]
    ]
];