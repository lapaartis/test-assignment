<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('user_tests', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->integer('user_id')->unsigned();
    $table->integer('test_id')->unsigned();
    $table->integer('right_answers')->default('0');
    $table->integer('total_answers')->default('0');
    $table->timestamps();

    // FOREIGN
    $table->foreign('test_id')->references('id')->on('tests');
    $table->foreign('user_id')->references('id')->on('users');

    // INDEXES
    $table->index(['test_id']);
    $table->index(['user_id', 'test_id']);
});
var_dump('Table created: User Tests');