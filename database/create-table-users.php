<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('users', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->string('name')->default('');
    $table->string('token')->nullable();
    $table->timestamps();

    // INDEXES
    $table->index(['name']);
});

var_dump('Table created: Users');
