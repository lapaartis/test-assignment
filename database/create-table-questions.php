<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('questions', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->string('name')->default('');
    $table->integer('test_id')->unsigned();
    $table->timestamps();

    // FOREIGN
    $table->foreign('test_id')->references('id')->on('tests');

    // INDEXES
    $table->index(['test_id']);
});
var_dump('Table created: Questions');
