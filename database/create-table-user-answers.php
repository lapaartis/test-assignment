<?php

use Illuminate\Database\Capsule\Manager;

Manager::schema()->create('user_answers', function ($table) {
    $table->charset = 'utf8';
    $table->collation = 'utf8_unicode_ci';
    $table->engine = 'InnoDB';

    $table->increments('id');
    $table->integer('user_test_id')->unsigned();
    $table->integer('question_id')->unsigned();
    $table->integer('answer_id')->unsigned();
    $table->timestamps();

    // FOREIGN
    $table->foreign('user_test_id')->references('id')->on('user_tests');
    $table->foreign('question_id')->references('id')->on('questions');
    $table->foreign('answer_id')->references('id')->on('answers');

    // INDEXES
    $table->index(['user_test_id', 'question_id']);
    $table->index(['user_test_id', 'answer_id']);
});
var_dump('Table created: User Answers');