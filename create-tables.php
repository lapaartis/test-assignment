<?php

require 'vendor/autoload.php';
require 'bootstrap/db.php';

include 'database/create-table-tests.php';
include 'database/create-table-users.php';
include 'database/create-table-questions.php';
include 'database/create-table-answers.php';
include 'database/create-table-user-tests.php';
include 'database/create-table-user-answers.php';

var_dump('Database tables created Succesfully!');

